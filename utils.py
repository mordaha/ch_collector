import datetime


def log_time_CHWriter(fn):
    def wraper(self, *args, **kw):
        tstart = datetime.datetime.now()
        fn(self, *args, **kw)
        tstop = datetime.datetime.now()

        diff = (tstop - tstart).seconds * 10**6 + (tstop - tstart).microseconds
        print("pkg's written: {} sec".format(diff / 10**6))
        self.alltime_posts += diff
        self.allcount_posts += 1

    return wraper
