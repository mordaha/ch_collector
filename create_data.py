import time
import json
import datetime
import random
from functools import lru_cache
from multiprocessing import Process

import postgresql
import requests
from infi.clickhouse_orm.database import Database

from utils import log_time_CHWriter


REGIONS = [77, 101, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254,
           256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277,
           278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299,
           300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 323]


class SimpleCHGenerator:
    pg_conn_line = 'pq://hc_market:hc_market@127.0.0.1:5432/hc_market'
    pg_conn = None
    start_ts = 1504285780.301077
    stop_ts = 1535695711.301077
    row_count = 10**8

    def __init__(self, row_count=None):
        self.pg_conn = postgresql.open(self.pg_conn_line)
        self.row_count = row_count or self.row_count
        self._ts_step = (self.stop_ts - self.start_ts) / self.row_count
        self.start_ts, self.stop_ts = self.stop_ts, self.start_ts
        self._ts_step = -1 * self._ts_step

    @staticmethod
    def _get_rand_in_limit(left, right) -> int:
        return int(left + random.random()*(right - left))

    @staticmethod
    def _get_rand_for_rating() -> int:
        # нужно сместить рейтинг в первую половину (или треть)
        return int(random.random() * (40 + random.random()*60))

    @lru_cache(maxsize=16)
    def _get_prod_set_by_rating(self, rating):
        return self.pg_conn.query("""
        select id from "product_item" where "rating" = {rating}
        """.format(
            rating=rating
        ))

    def _get_random_prod(self):
        prod_set = self._get_prod_set_by_rating(
            self._get_rand_for_rating()
        )
        while len(prod_set) == 0:
            prod_set = self._get_prod_set_by_rating(self._get_rand_for_rating())
        return prod_set[
            self._get_rand_in_limit(0, len(prod_set))
        ][0]

    @staticmethod
    def _get_random_region():
        # хочется МСК и СПБ чаще
        idx = random.choice([0, 1, 2])
        if idx < 2:
            return REGIONS[idx]
        return random.choice(REGIONS[2:])

    def gen_view_stat(self):
        # будем смешать таймстамп пока равномерно
        ts = self.start_ts
        clicked = random.choice([0, 1])
        app = random.choice([0, 1])
        liked = random.choice([0, 1])

        while ts >= self.stop_ts:
            dt = datetime.datetime.fromtimestamp(ts)
            yield {
                'product': self._get_random_prod(),
                'date': dt.date(),
                'time': dt.time(),
                'datetime': dt,
                'timestamp': ts,
                'clicked': clicked,
                'application': app,
                'liked': liked,
                'viewed': random.choice([0, 1]) if app == 1 else 1,
                'region': self._get_random_region(),
            }
            ts += self._ts_step


class CHWriter:
    _proc_count = 10

    ch_conn_line = 'http://127.0.0.1:8123'
    ch_client = None
    alltime_posts = 0
    allcount_posts = 1
    ch_orm_db = None

    def __init__(self, pkg_len=None):
        self.pkg_len = pkg_len or 100

    @log_time_CHWriter
    def _write_by_http(self, data):
        requests.post(
            self.ch_conn_line,
            data=data
        )

    @log_time_CHWriter
    def _write_by_ch_orm(self, data):
        if self.ch_orm_db is None:
            self.ch_orm_db = Database('test_rating')
        self.ch_orm_db.raw(
            data
        )

    def _run(self, i_gen, write_method):
        pkg = []
        count = 0
        for i in i_gen.gen_view_stat():
            pkg.append("""
            ('{product}', '{date}', '{dt}', {region}, {clicked}, {app}, {liked}, {viewed}) 
            """.format(
                product=i['product'],
                date=i['date'].strftime('%Y-%m-%d'),
                dt=i['datetime'].strftime('%Y-%m-%d %H:%M:%S'),
                region=i['region'],
                clicked=i['clicked'],
                app=i['application'],
                liked=i['liked'],
                viewed=i['viewed'],
            ))
            count += 1
            if len(pkg) == self.pkg_len:
                str_data = """
                    insert into test_rating.product (
                        product, sdate, sdatetime, region, clicked, application, liked, viewed
                    ) values {!s}
                """.format(', '.join(pkg))
                write_method(str_data)
                pkg = []

    def _stress_run(self, i_gen):
        for i in i_gen.gen_view_stat():
            data = dict(
                product=str(i['product']),
                date=i['date'].strftime('%Y-%m-%d'),
                dt=i['datetime'].strftime('%Y-%m-%d %H:%M:%S'),
                region=i['region'],
                clicked=i['clicked'],
                app=i['application'],
                liked=i['liked'],
                viewed=i['viewed'],
            )
            requests.post(
                'http://127.0.0.1:8080/{}'.format(random.choice(['push_profile', 'push_rating'])),
                data={'data': json.dumps(data)}
            )
            time.sleep(random.choice([.50, .10, .500]))

    def run_in_proccesses(self):
        procs = []
        for index in range(self._proc_count):
            i_gen = SimpleCHGenerator(row_count=10 ** 6)
            proc = Process(target=self._stress_run, args=(i_gen,))
            procs.append(proc)
            proc.start()

        for proc in procs:
            proc.join()

    def run_with_post(self):
        for i in range(3):
            self.pkg_len = 10**(i+1)
            i_gen = SimpleCHGenerator(row_count=self.pkg_len*100)
            self._run(i_gen, self._write_by_http)
            print("avg time {} sec".format(
                (self.alltime_posts / self.allcount_posts) / 1000
            ))
            self.alltime_posts = self.allcount_posts = 0

    def run_with_ch_orm(self):
        for i in range(3):
            self.pkg_len = 10**(i+1)
            i_gen = SimpleCHGenerator(row_count=self.pkg_len*100)
            self._run(i_gen, self._write_by_ch_orm)
            print("avg time {} sec".format(
                (self.alltime_posts / self.allcount_posts) / 1000
            ))
            self.alltime_posts = self.allcount_posts = 0


if __name__ == '__main__':
    CHWriter().run_in_proccesses()
