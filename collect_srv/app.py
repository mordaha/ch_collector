import asyncio

from aiohttp import web

from collect_srv.collector import MetricsCollector


class CollectorApplication(web.Application):
    __icollector = MetricsCollector()

    @property
    def icollector(self):
        return self.__icollector

    async def push_rating(self, request):
        return await self.icollector.collect('rating', request)

    async def push_profile(self, request):
        return await self.icollector.collect('profile', request)

    async def startup(self):
        await super(CollectorApplication, self).startup()
        asyncio.ensure_future(self.icollector.flush())


async def get_collect_application():
    loop = asyncio.get_event_loop()
    app = CollectorApplication(loop=loop)

    app.add_routes([
        web.post('/push_rating', app.push_rating),
        web.post('/push_profile', app.push_profile)
    ])

    return app


# if __name__ == '__main__':
#     web.run_app(get_collect_application())
