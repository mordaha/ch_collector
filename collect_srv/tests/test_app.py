import json
import random
import datetime
import asyncio

import requests
from aiohttp.test_utils import AioHTTPTestCase
from aiohttp.test_utils import unittest_run_loop

from collect_srv.app import get_collect_application

CLICKHOUSE_CONN_LINE = 'http://127.0.0.1:8123'
REGIONS = [77, 101, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254,
           256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277,
           278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299,
           300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 323]


class MyAppTestCase(AioHTTPTestCase):
    async def get_application(self):
        return await get_collect_application()

    @staticmethod
    def make_data():
        dt = datetime.datetime.now()
        clicked = random.choice([0, 1])
        app = random.choice([0, 1])
        liked = random.choice([0, 1])

        return {
            'product': '72ef4c15-620f-4f6c-a032-6c6488263b2',
            'date': dt.strftime('%Y-%m-%d'),
            'dt': dt.strftime('%Y-%m-%d %H:%M:%S'),
            'clicked': clicked,
            'app': app,
            'liked': liked,
            'viewed': random.choice([0, 1]) if app == 1 else 1,
            'region': random.choice(REGIONS),
        }

    @staticmethod
    def ask_data_from_clickhouse(table, data=None):
        response = requests.post(
            CLICKHOUSE_CONN_LINE,
            data="""
                select count() from test_rating.{table} {cond}
            """.format(
                cond="where product = '{prod}' and sdatetime = '{dt}'".format(
                    prod=data['product'], dt=data['datetime']
                ) if data else '',
                table=table
            ))
        return response.json()

    def get_pkg_len_by_metric(self, metric):
        """так делать не хорошо, но будем проверять пачку на запись через протектид атрибуты"""
        return len(self.app.icollector._proxy_metrics[metric]['pkg'])

    @unittest_run_loop
    async def test_put_empty_data(self):
        """проверим, что невалидные данные вы райзят ошибок"""
        response = await self.client.request("POST", "/push_rating")

        assert response.status == 200
        text = await response.text()

        assert "OK" in text.upper()

    @unittest_run_loop
    async def test_put_rating(self):
        """чекнем, что данные пушатся в нужную метрику"""
        data = self.make_data()
        old_pkg_count = self.get_pkg_len_by_metric('rating')

        response = await self.client.request(
            "POST", "/push_rating",
            data={'data': json.dumps(data)}
        )

        assert response.status == 200
        text = await response.text()

        assert "OK" in text.upper()
        assert self.get_pkg_len_by_metric('rating') > old_pkg_count

    @unittest_run_loop
    async def test_put_profile(self):
        """чекнем, что данные пушатся в нужную метрику"""
        data = self.make_data()
        old_pkg_count = self.get_pkg_len_by_metric('profile')

        response = await self.client.request(
            "POST", "/push_profile",
            data={'data': json.dumps(data)}
        )

        assert response.status == 200
        text = await response.text()

        assert "OK" in text.upper()
        assert self.get_pkg_len_by_metric('profile') > old_pkg_count

    @unittest_run_loop
    async def test_flush(self):
        """проверим, что данные флашатся в CH"""
        len_profile = self.ask_data_from_clickhouse('product1')

        self.app.icollector._proxy_metrics['profile']['pkg'] = [
            self.make_data()
        ]
        self.app.icollector._proxy_metrics['profile']['lasttime_flush'] = datetime.datetime.now() + datetime.timedelta(seconds=-40)
        await self.app.icollector.flush()

        asyncio.sleep(20)

        assert len_profile < self.ask_data_from_clickhouse('product1')


if __name__ == '__main__':
    MyAppTestCase.run()
