import json
import asyncio
import datetime
from copy import deepcopy

import requests
from aiohttp import web


class MetricsCollector:
    __inst_collector = None
    __proxy_structure = {
        'pkg': [],
        'lasttime_flush': None,
        'table': None
    }
    __is_run = True

    _flush_time = 20
    _flush_size = 100

    _metrics_list = [
        'profile',
        'rating',
    ]

    _map_metric_to_table = {
        'profile': 'product1',
        'rating': 'product',
    }

    _proxy_metrics = {}

    def __new__(cls, *args, **kw):
        if cls.__inst_collector is None:
            cls.__inst_collector = super(MetricsCollector, cls).__new__(cls, *args, **kw)

            for m in cls._metrics_list:
                cls._proxy_metrics[m] = deepcopy(cls.__proxy_structure)
                cls._proxy_metrics[m]['table'] = cls._map_metric_to_table[m]

        return cls.__inst_collector

    def _get_metric(self, metric, data):
        if data:
            self._proxy_metrics[metric]['pkg'].append(
                data
            )

    async def _return_ok(self):
        return web.Response(text='Ok')

    async def collect(self, metric, request):
        """
        Собираем данные по метрике

        :param metric(str): название метрики
        :param data(dict): данные для вставки

        :return: None
        """
        if metric not in self._metrics_list:
            return self._return_ok()

        data = (await request.post()).get('data')
        if data:
            data = json.loads(data)
            self._get_metric(metric, data)

        return await self._return_ok()

    @classmethod
    async def run(cls):
        while True:
            await cls.flush()
            await asyncio.sleep(15)

    @classmethod
    async def flush(cls):
        for m in cls._proxy_metrics:
            if cls._proxy_metrics[m]['lasttime_flush'] is None:
                cls._proxy_metrics[m]['lasttime_flush'] = deepcopy(datetime.datetime.now())

            if (datetime.datetime.now() - cls._proxy_metrics[m]['lasttime_flush']).seconds >= cls._flush_time \
                or len(cls._proxy_metrics[m]['pkg']) >= cls._flush_size:
                print('FLUSH')

                if not cls._proxy_metrics[m]['pkg']:
                    continue

                data = """
                        insert into test_rating.{!s} (
                        product, sdate, sdatetime, region, clicked, application, liked, viewed
                    ) values {!s}
                    """.format(
                        cls._proxy_metrics[m]['table'],
                        ", ".join(
                            map(
                                lambda i: "('{product}', '{date}', '{dt}', {region}, {clicked}, {app}, {liked}, {viewed})" .format(
                                        product=i['product'],
                                        date=i['date'],
                                        dt=i['dt'],
                                        region=i['region'],
                                        clicked=i['clicked'],
                                        app=i['app'],
                                        liked=i['liked'],
                                        viewed=i['viewed'],
                                    ),
                                cls._proxy_metrics[m]['pkg']
                            )
                        )
                    )
                r = requests.post('http://127.0.0.1:8123/', data)
                print(r)

                cls._proxy_metrics[m]['lasttime_flush'] = datetime.datetime.now()
                cls._proxy_metrics[m]['pkg'] = []

        await asyncio.sleep(10)
        asyncio.ensure_future(cls.flush())
